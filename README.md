This is a Spring/JSF CRUD application made in preparation for a Java Developer qualification exam. Specs include:
1. no JSF annotations, only Spring beans, configured in XML
2. logging creation and destruction of every bean
3. JPA with H2 database, using EntityManager, no annotations
4. all UI beans must be prototype scoped
5. the application features one Validator and one Converter in the UI package
6. there is also a line chart and an EAN13 barcode (PrimeFaces components)