package itakademija.java2015.myapp.entities.repositories.jpa;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.Item;
import itakademija.java2015.myapp.entities.repositories.ItemRepository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemRepositoryJPA implements ItemRepository {

    static final Logger log = LoggerFactory.getLogger(CouponRepositoryJPA.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private EntityManagerFactory entityManagerFactory;

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.entityManagerFactory = emf;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public void save(Item item) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!entityManager.contains(item) && item.getId() != null) {
                item = entityManager.merge(item);
            }
            entityManager.persist(item);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void delete(Item item) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            item = entityManager.merge(item);
            entityManager.remove(item);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Item> findAll() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Item> cq = cb.createQuery(Item.class);
            Root<Item> root = cq.from(Item.class);
            cq.select(root);
            TypedQuery<Item> q = entityManager.createQuery(cq);
            return q.getResultList();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public List<Item> findAllByCoupon(Coupon coupon) {
        EntityManager entityManager = getEntityManager();
        try {
            coupon = entityManager.merge(coupon);
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Item> cq = cb.createQuery(Item.class);
            Root<Item> root = cq.from(Item.class);
            cq.select(root);
            cq.where(cb.equal(root.get("coupon"), coupon));
            TypedQuery<Item> q = entityManager.createQuery(cq);
            return q.getResultList();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Number countItemsByPriceInterval(int min, int max,
            boolean countSpecial, int specialThreshold) {
        EntityManager entityManager = getEntityManager();
        String jpql = "";
        if (countSpecial) {
            jpql = "Select SUM(i.amount) from Item i WHERE i.amount <= :specialThreshold AND i.price BETWEEN :min AND :max";
        } else {
            jpql = "Select sum(i.amount) from Item i WHERE i.amount > :specialThreshold AND i.price BETWEEN :min AND :max";
        }
        try {
            TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
            query.setParameter("specialThreshold", specialThreshold);
            query.setParameter("min", new BigDecimal(min));
            query.setParameter("max", new BigDecimal(max));
            if (query.getSingleResult() == null) {
                return 0;
            } else {
                return query.getSingleResult().intValue();
            }
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long countItemsByCouponId(Coupon coupon) {
        EntityManager entityManager = getEntityManager();
        try {
            coupon = entityManager.merge(coupon);
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<Item> root = cq.from(Item.class);
            cq.select(cb.count(root));
            cq.where(cb.equal(root.get("coupon"), coupon));
            TypedQuery<Long> q = entityManager.createQuery(cq);
            return q.getSingleResult();
        } finally {
            entityManager.close();
        }
    }

}
