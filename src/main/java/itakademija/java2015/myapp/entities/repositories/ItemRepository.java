package itakademija.java2015.myapp.entities.repositories;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.Item;

import java.util.List;

public interface ItemRepository {

    public void save(Item newCoupon);

    public void delete(Item item);

    public List<Item> findAll();

    public List<Item> findAllByCoupon(Coupon coupon);

    public Number countItemsByPriceInterval(int min, int max,
            boolean countSpecial, int specialThreshold);

    public Long countItemsByCouponId(Coupon coupon);

}
