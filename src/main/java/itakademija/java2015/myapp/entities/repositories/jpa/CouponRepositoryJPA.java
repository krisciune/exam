package itakademija.java2015.myapp.entities.repositories.jpa;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.repositories.CouponRepository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponRepositoryJPA implements CouponRepository {

    static final Logger log = LoggerFactory.getLogger(CouponRepositoryJPA.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private EntityManagerFactory entityManagerFactory;

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.entityManagerFactory = emf;
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public void save(Coupon coupon) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!entityManager.contains(coupon))
                coupon = entityManager.merge(coupon);
            entityManager.persist(coupon);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void delete(Coupon coupon) {
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            coupon = entityManager.merge(coupon);
            entityManager.remove(coupon);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public List<Coupon> findAll() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
            Root<Coupon> root = cq.from(Coupon.class);
            cq.select(root);
            TypedQuery<Coupon> q = entityManager.createQuery(cq);
            return q.getResultList();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long countCoupons() {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<Coupon> root = cq.from(Coupon.class);
            cq.select(cb.count(root));
            TypedQuery<Long> q = entityManager.createQuery(cq);
            return q.getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Coupon findById(Long id) {
        EntityManager entityManager = getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
            Root<Coupon> root = cq.from(Coupon.class);
            cq.where(cb.equal(root.get("id"), id));
            TypedQuery<Coupon> q = entityManager.createQuery(cq);
            return q.getSingleResult();
        } finally {
            entityManager.close();
        }
    }


}
