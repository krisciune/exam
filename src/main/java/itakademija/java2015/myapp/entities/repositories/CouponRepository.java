package itakademija.java2015.myapp.entities.repositories;

import itakademija.java2015.myapp.entities.Coupon;

import java.util.List;

public interface CouponRepository {

    void save(Coupon newCoupon);

    void delete(Coupon coupon);

    List<Coupon> findAll();

    Coupon findById(Long id);

    Long countCoupons();

}
