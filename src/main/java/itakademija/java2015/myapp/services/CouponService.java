package itakademija.java2015.myapp.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.repositories.CouponRepository;

public class CouponService {

    static final Logger log = LoggerFactory.getLogger(CouponService.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private CouponRepository couponRepo;

    public void delete(Coupon coupon) {
        couponRepo.delete(coupon);
    }

    public List<Coupon> findAll() {
        return couponRepo.findAll();
    }

    public Coupon findById(Long currentCouponId) {
        return couponRepo.findById(currentCouponId);
    }

    public void save(Coupon currentCoupon) {
        couponRepo.save(currentCoupon);
    }

    public CouponRepository getCouponRepo() {
        return couponRepo;
    }

    public void setCouponRepo(CouponRepository couponRepo) {
        this.couponRepo = couponRepo;
    }

    public Long countCoupons() {
        return couponRepo.countCoupons();
    }

}
