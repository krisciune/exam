package itakademija.java2015.myapp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.Item;
import itakademija.java2015.myapp.entities.repositories.ItemRepository;

public class ItemService {

    static final Logger log = LoggerFactory.getLogger(ItemService.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private ItemRepository itemRepo;

    public void save(Item item) {
        itemRepo.save(item);
    }

    public void delete(Item item) {
        itemRepo.delete(item);
    }

    public ItemRepository getItemRepo() {
        return itemRepo;
    }

    public void setItemRepo(ItemRepository itemRepo) {
        this.itemRepo = itemRepo;
    }

    public Number countItemsByPriceInterval(int min, int max, boolean countSpecial,
            int specialThreshold) {
        return itemRepo.countItemsByPriceInterval(min, max, countSpecial, specialThreshold);
    }

    public Long countItemsByCouponId(Coupon coupon) {
        return itemRepo.countItemsByCouponId(coupon);
    }

}
