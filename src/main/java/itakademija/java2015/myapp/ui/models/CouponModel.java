package itakademija.java2015.myapp.ui.models;

import itakademija.java2015.myapp.entities.Coupon;

import java.io.Serializable;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponModel implements Serializable {

    private static final long serialVersionUID = 5040310448257955354L;

    static final Logger log = LoggerFactory.getLogger(CouponModel.class);

    public void destroy() {
        log.warn("Bean destroyed");
    }

    @Valid
    private Coupon currentCoupon;

    public void init() {
        log.warn("Bean created");
        currentCoupon = new Coupon();
    }

    public Coupon getCurrentCoupon() {
        return currentCoupon;
    }

    public void setCurrentCoupon(Coupon currentCoupon) {
        this.currentCoupon = currentCoupon;
    }

}