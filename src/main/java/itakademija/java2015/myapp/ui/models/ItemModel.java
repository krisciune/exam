package itakademija.java2015.myapp.ui.models;

import itakademija.java2015.myapp.entities.Item;

import java.io.Serializable;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemModel implements Serializable {

    private static final long serialVersionUID = -8131673305859876205L;

    static final Logger log = LoggerFactory.getLogger(CouponModel.class);

    public void destroy() {
        log.warn("Bean destroyed");
    }

    @Valid
    private Item currentItem;

    public void init() {
        log.warn("Bean created");
        currentItem = new Item();
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(Item currentItem) {
        this.currentItem = currentItem;
    }
}
