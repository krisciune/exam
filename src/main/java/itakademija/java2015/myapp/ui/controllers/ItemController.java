package itakademija.java2015.myapp.ui.controllers;

import itakademija.java2015.myapp.entities.Item;
import itakademija.java2015.myapp.services.ItemService;
import itakademija.java2015.myapp.ui.models.CouponModel;
import itakademija.java2015.myapp.ui.models.ItemModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemController implements Serializable {

    private static final long serialVersionUID = -1389684115871062390L;

    static final Logger log = LoggerFactory.getLogger(ItemController.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private CouponModel couponModel;
    private ItemModel itemModel;
    private ItemService itemService;

    public BigDecimal calculateItemTotal(Item item) {
        BigDecimal itemTotal = item.getPrice().multiply(new BigDecimal(item.getAmount()));
        return itemTotal;
    }

    public LineChartModel calculateLineModel() {
        int specialThreshold = 2;
        LineChartModel model = new LineChartModel();
        ChartSeries specialItems = new ChartSeries();
        specialItems.setLabel("Special Items (2 or less)");
        specialItems.set("0-9", itemService.countItemsByPriceInterval(0, 9, true, specialThreshold));
        specialItems.set("10-49", itemService.countItemsByPriceInterval(10, 49, true, specialThreshold));
        specialItems.set("50-99", itemService.countItemsByPriceInterval(50, 99, true, specialThreshold));
        specialItems.set("100-3455", itemService.countItemsByPriceInterval(100, 3455, true, specialThreshold));
        specialItems.set("3456 and more", itemService.countItemsByPriceInterval(3456, Integer.MAX_VALUE, true, specialThreshold));
        ChartSeries massProdItems = new ChartSeries();
        massProdItems.setLabel("Mass Produced Items (3 or more)");
        massProdItems.set("0-9", itemService.countItemsByPriceInterval(0, 9, false, specialThreshold));
        massProdItems.set("10-49", itemService.countItemsByPriceInterval(10, 49, false, specialThreshold));
        massProdItems.set("50-99", itemService.countItemsByPriceInterval(50, 99, false, specialThreshold));
        massProdItems.set("100-3455", itemService.countItemsByPriceInterval(100, 3455, false, specialThreshold));
        massProdItems.set("3456 and more", itemService.countItemsByPriceInterval(3456, Integer.MAX_VALUE, false, specialThreshold));
        model.addSeries(specialItems);
        model.addSeries(massProdItems);
        model.setTitle("Items by Price Intervals");
        model.setLegendPosition("e");
        model.setShowPointLabels(true);
        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setLabel("Units");
        yAxis.setMin(0);
        int max = 40;
        max = adjustMaxForDataset(max, specialItems.getData());
        max = adjustMaxForDataset(max, massProdItems.getData());
        yAxis.setMax(max);
        model.getAxes().put(AxisType.X, new CategoryAxis("Price Intervals"));
        return model;
    }

    private int adjustMaxForDataset(int max, Map<Object, Number> data) {
        for (Number num : data.values()) {
            if (max - 10 < (Integer) num) {
                max = (Integer) num + 10;
            }
        }
        return max;
    }

    public void create() {
        itemModel.setCurrentItem(new Item());
    }

    public void update(Item item) {
        itemModel.setCurrentItem(item);
    }

    public String save() {
        itemModel.getCurrentItem().setCoupon(couponModel.getCurrentCoupon());
        itemService.save(itemModel.getCurrentItem());
        itemModel.setCurrentItem(new Item());
        return CouponController.NAV_LIST_ITEMS;
    }

    public void updateGrowl() {
        if (couponModel == null || couponModel.getCurrentCoupon() == null) {
            return;
        }
        Long itemCount = itemService.countItemsByCouponId(couponModel.getCurrentCoupon());
        if (itemCount == 0) {
            FacesMessage warningMessage = new FacesMessage("A coupon must have at least 1 item");
            FacesContext.getCurrentInstance().addMessage(null, warningMessage);
        }
    }

    public String delete(Item item) {
        itemService.delete(item);
        updateGrowl();
        return CouponController.NAV_LIST_ITEMS;
    }

    public String cancel() {
        itemModel.setCurrentItem(new Item());
        return CouponController.NAV_LIST_ITEMS;
    }

    public ItemModel getItemModel() {
        return itemModel;
    }
    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public CouponModel getCouponModel() {
        return couponModel;
    }

    public void setCouponModel(CouponModel couponModel) {
        this.couponModel = couponModel;
    }
}
