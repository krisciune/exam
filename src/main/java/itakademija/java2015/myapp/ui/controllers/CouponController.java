package itakademija.java2015.myapp.ui.controllers;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.entities.Item;
import itakademija.java2015.myapp.services.CouponService;
import itakademija.java2015.myapp.ui.models.CouponModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponController implements Serializable {

    private static final long serialVersionUID = -8143077418240345336L;

    static final Logger log = LoggerFactory.getLogger(CouponController.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    public static final String NAV_LIST_COUPONS = "list-coupons";
    public static final String NAV_LIST_ITEMS = "list-items";
    public static final String EUR_TO_LT = "3.4528";

    private CouponModel couponModel;
    private CouponService couponService;

    public BigDecimal calculateCouponTotal(Coupon coupon) {
        BigDecimal couponTotal = BigDecimal.ZERO;
        List<Item> items = coupon.getItems();
        for (Item item : items) {
            BigDecimal itemTotal = item.getPrice().multiply(new BigDecimal(item.getAmount()));
            couponTotal = couponTotal.add(itemTotal);
        }
        return couponTotal;
    }

    public BigDecimal calculateTotalInLT(Coupon coupon) {
        BigDecimal couponTotal = calculateCouponTotal(coupon);
        couponTotal = couponTotal.multiply(new BigDecimal(EUR_TO_LT));
        couponTotal = couponTotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        return couponTotal;
    }

    public String calculateBarcode() {
        int barcodeLengthNoChecksum = 12;
        String barcode = "00000000";
        if (couponModel != null && couponModel.getCurrentCoupon() != null) {
            Date couponDate = couponModel.getCurrentCoupon().getCouponDate();
            if (couponDate != null) {
                barcode = String.valueOf(couponDate.getTime()).substring(0, 8);
            }
            try {
                int id = Integer.parseInt(couponModel.getCurrentCoupon().getSerial());
                barcode = barcode + id;
            } catch (Exception ex) {
                log.debug("Serial not a number, can't use for barcode");
            }
            if (barcode.length() < barcodeLengthNoChecksum) {
                int addZeroes = barcodeLengthNoChecksum - barcode.length();
                for (int i = 0; i < addZeroes; i++) {
                    barcode = barcode + "0";
                }
            } else if (barcode.length() > barcodeLengthNoChecksum) {
                barcode = barcode.substring(0, barcodeLengthNoChecksum);
            }
        }
        return barcode;
    }

    public void updateGrowl() {
        Long couponCount = couponService.countCoupons();
        if (couponCount < 2) {
            FacesMessage warningMessage = new FacesMessage("At least 2 coupons are required");
            FacesContext.getCurrentInstance().addMessage(null, warningMessage);
        }
    }

    public void create() {
        couponModel.setCurrentCoupon(new Coupon());
    }

    public String delete(Coupon coupon) {
        couponService.delete(coupon);
        return NAV_LIST_COUPONS;
    }

    public List<Coupon> getCouponList() {
        return couponService.findAll();
    }

    public List<Item> findItemsByCoupon() {
        return couponModel.getCurrentCoupon().getItems();
    }

    public List<Item> getFreshItemList() {
        Long currentCouponId = couponModel.getCurrentCoupon().getId();
        if (currentCouponId == null) return null;
        return couponService.findById(currentCouponId).getItems();
    }

    public String update(Coupon coupon) {
        couponModel.setCurrentCoupon(coupon);
        return NAV_LIST_ITEMS;
    }

    public String save() {
        couponService.save(couponModel.getCurrentCoupon());
        return NAV_LIST_COUPONS;
    }

    public String cancel() {
        couponModel.setCurrentCoupon(new Coupon());
        return NAV_LIST_COUPONS;
    }

    public CouponModel getCouponModel() {
        return couponModel;
    }

    public void setCouponModel(CouponModel couponModel) {
        this.couponModel = couponModel;
    }

    public CouponService getCouponService() {
        return couponService;
    }

    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

}
