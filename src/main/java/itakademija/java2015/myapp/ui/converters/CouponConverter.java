package itakademija.java2015.myapp.ui.converters;

import itakademija.java2015.myapp.entities.Coupon;
import itakademija.java2015.myapp.services.CouponService;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponConverter implements Converter {

    static final Logger log = LoggerFactory.getLogger(CouponConverter.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    private CouponService couponService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            Object obj =  couponService.findById(Long.valueOf(value));
            return obj;
        } catch (Exception e) {
            throw new ConverterException(new FacesMessage(String.format("Cannot convert %s to Coupon", value)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Coupon)) {
            return null;
        }
       String s = String.valueOf(((Coupon) value).getId());
       return s;
    }

    public CouponService getCouponService() {
        return couponService;
    }

    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

}
