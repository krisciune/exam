package itakademija.java2015.myapp.ui.validators;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositiveNumberValidator implements Validator {

    private static final Logger log = LoggerFactory.getLogger(PositiveNumberValidator.class);

    public void init() {
        log.warn("Bean created");
    }

    public void destroy() {
        log.warn("Bean destroyed");
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        int amount = 0;
        double doubleAmount = 0;
        if (value instanceof BigDecimal) {
            BigDecimal decimalAmount = (BigDecimal) value;
            doubleAmount = decimalAmount.doubleValue();
        } else if (value instanceof Double){
            doubleAmount = (double) value;
        } else if (value instanceof Integer) {
            amount = (Integer) value;
        } else {
            FacesMessage message = new FacesMessage("Not a number");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

        if (amount < 0 || doubleAmount < 0) {
            FacesMessage message = new FacesMessage("Cannot use negative number");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}
